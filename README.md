This is an example repo to demonstrate how to enable continuous integration of
LaTeX projects using GitLab Pages. Read more about it on my
[blog](https://www.vipinajayakumar.com/continuous-integration-of-latex-projects-with-gitlab-pages.html).